""" 
Implementaion of the Queue data structure in Python

Methods in the Implementation

1. Check if Queue is Empty
2. Check the size of the queue
3. Enqueue items to the queue
4. Check the front/top of the queue
5. Dequeue from the queue

"""

class Queue:

    def __init__(self):
        self.queue = []

    def is_empty(self):
        return self.queue == []

    def size(self):
        if self.is_empty():
            return None
        else:
            return len(self.queue)

    def enqueue(self, value):
        self.queue.insert(0, value)

    def dequeue(self):
        self.queue.pop()

    def top_of_queue(self):
        return self.queue[-1]

    def __str__(self):
        return " -> ".join([str(x) for x in self.queue])



#  Sample Implementation
my_queue = Queue()
print(my_queue.is_empty())
print(my_queue.size())

# Enqueue Items
my_queue.enqueue("apples")
my_queue.enqueue("oranges")
my_queue.enqueue("pineapples")

#  Print Queue
print(my_queue)

#  Return Top of Queue
print(my_queue.top_of_queue())

#  Dequeue
my_queue.dequeue()
print(my_queue)
